# Ionic starter project

In this repo, we develop the Ionic starter project with the following features:

1. Capacitor
2. React
3. Auto incremental version number
4. Autoupdate Android condig

---

## Requirements

For development, you will need Node.js

## Getting Started

```sh
npn run build.dev
